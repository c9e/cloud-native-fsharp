module Cxe.Sabine.Web.Functions

open System.Net
open System.IO
open Microsoft.AspNetCore.Http
open Microsoft.Azure.Functions.Worker
open Microsoft.Extensions.Logging
open Microsoft.Azure.Functions.Worker.Http


[<Function("Index")>]
let run ([<HttpTrigger(AuthorizationLevel.Function, "get", Route = null)>]req: HttpRequestData) (executionContext:FunctionContext) =
    async {
        let log = executionContext.GetLogger("Index")
        log.LogInformation("F# HTTP trigger function processed a request.")

        let responseMessage =  "This HTTP triggered function executed successfully."

        let resp = req.CreateResponse(HttpStatusCode.OK);
        do! resp.WriteStringAsync(responseMessage) |> Async.AwaitTask

        return resp
    } |> Async.StartAsTask